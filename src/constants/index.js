export const IMAGES_LOAD = "IMAGES_LOAD";
export const LOAD_SUCCESS = "LOAD_SUCCESS";
export const LOAD_FAIL = "LOAD_FAIL";
export const LOAD_STATS = "LOAD_STATS";
export const LOAD_SUCCESS_STATS = "LOAD_SUCCESS_STATS";
export const LOAD_FAIL_STATS = "LOAD_FAIL_STATS";
