import React from "react";

import "./stats.css";

const States = ({ stats }) => {
  if (!stats) {
    return <span className="stats">Loading...</span>;
  }
  return (
    <span className="stats">
      {stats.error && "🤯 Error!"}
      {stats.isLoading && "🙄 Loading..."}
      {stats.downloads && `🤘 ${stats.downloads}`}
    </span>
  );
};

export default States;
