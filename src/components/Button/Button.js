import React from "react";
import "./button.css";

const Button = ({ loading, children, ...props }) => {
  return (
    <button className="button" disabled={loading} {...props}>
      {loading ? "Loading..." : children}
    </button>
  );
};

Button.defaultProps = {
  loading: false,
};

export default Button;
