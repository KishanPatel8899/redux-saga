import React, { useEffect } from "react";
import { LoadImage } from "../../actions";
import { useDispatch, useSelector } from "react-redux";
import Button from "../Button";
import States from "../stats";

import "./style.css";

const ImageGrid = () => {
  const dispatch = useDispatch();
  const { error, images, isLoading, imagesState } = useSelector(
    (state) => state
  );

  useEffect(() => {
    dispatch(LoadImage());
  }, []);

  if (error) {
    return (
      <div className="center error">
        Oops Error To Fetch
        <p>What server says {error}</p>
      </div>
    );
  }

  const LoadMoreImage = (e) => {
    if (!isLoading) {
      dispatch(LoadImage());
    }
  };

  return (
    <div className="content">
      <section className="grid">
        {images &&
          images.map((image) => (
            <div
              key={image.id}
              className={`item item-${Math.ceil(image.height / image.width)}`}
            >
              <States stats={imagesState[image.id]} />
              <img src={image.urls.small} alt={image.user.username} />
            </div>
          ))}
      </section>
      <Button loading={isLoading} onClick={() => LoadMoreImage()}>
        Load Images
      </Button>
    </div>
  );
};

export default ImageGrid;
