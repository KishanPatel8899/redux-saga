import React from "react";
import Header from "./components/header";
import ImageGrid from "./components/imagesGrid";

const App = () => {
  return (
    <div className="App">
      <Header />
      <ImageGrid />
    </div>
  );
};

export default App;
