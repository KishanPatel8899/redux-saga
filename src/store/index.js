import { createStore, applyMiddleware } from "redux";
import { rootReducer } from "../reducers";
import { composeWithDevTools } from "redux-devtools-extension";
import createSagaMiddleware from "redux-saga";
import rootSaga from "../sagas";

const configurestore = () => {
  const SagaMiddleware = createSagaMiddleware();
  const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(SagaMiddleware))
  );
  SagaMiddleware.run(rootSaga);

  return store;
};
export default configurestore;
