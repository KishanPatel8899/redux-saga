import * as Types from "../constants";

export const ImagesReducer = (state = [], action) => {
  switch (action.type) {
    case Types.LOAD_SUCCESS:
      return [...state, ...action.images];
    default:
      return state;
  }
};
