import { combineReducers } from "redux";
import { ImagesReducer } from "./Images.Reducer";
import { pageReducer } from "./Page.Reducer";
import { errorReducer } from "./errorReducer";
import { loadingReducer } from "./loadingReducer";
import { statsReducer } from "./statsReducers";

export const rootReducer = combineReducers({
  images: ImagesReducer,
  nextPage: pageReducer,
  isLoading: loadingReducer,
  error: errorReducer,
  imagesState: statsReducer,
});
