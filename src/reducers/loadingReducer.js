import * as Types from "../constants";

export const loadingReducer = (state = false, action) => {
  switch (action.type) {
    case Types.IMAGES_LOAD:
      return true;
    case Types.LOAD_SUCCESS:
      return false;
    case Types.LOAD_FAIL:
      return false;
    default:
      return state;
  }
};
