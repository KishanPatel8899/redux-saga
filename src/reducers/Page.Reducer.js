import { IMAGES_LOAD, LOAD_FAIL, LOAD_SUCCESS } from "../constants/index";

export const pageReducer = (state = 1, action) => {
  const { type, payload } = action;
  switch (type) {
    case LOAD_SUCCESS:
      return state + 1;
    default:
      return state;
  }
};
