import * as Types from "../constants";

export const errorReducer = (state = null, action) => {
  switch (action.type) {
    case Types.LOAD_FAIL:
      return action.error;
    case Types.IMAGES_LOAD:
    case Types.LOAD_SUCCESS:
      return null;
    default:
      return state;
  }
};
