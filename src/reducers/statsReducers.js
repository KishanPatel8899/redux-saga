import {
  LOAD_STATS,
  LOAD_FAIL_STATS,
  LOAD_SUCCESS_STATS,
} from "../constants/index";

export const statsReducer = (state = {}, action) => {
  switch (action.type) {
    case LOAD_STATS:
      return {
        ...state,
        [action.id]: {
          isLoading: true,
          downloads: null,
          error: false,
        },
      };
    case LOAD_SUCCESS_STATS:
      return {
        ...state,
        [action.id]: {
          isLoading: false,
          downloads: action.downloads,
          error: false,
        },
      };
    case LOAD_FAIL_STATS: {
      return {
        ...state,
        [action.id]: {
          isLoading: false,
          downloads: null,
          error: true,
        },
      };
    }
    default:
      return state;
  }
};
