import { all } from "redux-saga/effects";
import ImageSaga from "./ImagesSaga";
import StateSaga from "./statesSaga";

function* rootSage() {
  yield all([ImageSaga(), StateSaga()]);
}

export default rootSage;
