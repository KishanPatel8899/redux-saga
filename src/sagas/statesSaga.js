import { take, fork, put, call } from "redux-saga/effects";
import * as types from "../constants";
import { fetchImageState } from "../api";
import { loadImageStats, setImageStats, setImageStatsError } from "../actions";

function* handleStatsRequest(id) {
  for (let i = 0; i < 3; i++) {
    try {
      yield put(loadImageStats(id));
      const res = yield call(fetchImageState, id);
      yield put(setImageStats(id, res.downloads.total));
      return true;
    } catch (error) {
      console.log(error);
    }
  }
  yield put(setImageStatsError(id));
}

export default function* watchStatsRequest() {
  while (true) {
    const { images } = yield take(types.LOAD_SUCCESS);
    for (let i = 0; i < images.length; i++) {
      yield fork(handleStatsRequest, images[i].id);
    }
  }
}
