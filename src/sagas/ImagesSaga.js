import { takeEvery, select, call, put } from "redux-saga/effects";

import { fetchDataFunction } from "../api";
import { IMAGES_LOAD } from "../constants";
import { LoadImageSucces, LoadImageFail } from "../actions";

export const getPage = (state) => state.nextPage;

function* handleImageLoad() {
  try {
    const page = yield select(getPage);
    const images = yield call(fetchDataFunction, page);
    yield put(LoadImageSucces(images));
  } catch (error) {
    yield put(LoadImageFail(error.toString()));
  }
}

function* WatchKLoadImageSaga() {
  yield takeEvery(IMAGES_LOAD, handleImageLoad);
}

export default WatchKLoadImageSaga;
