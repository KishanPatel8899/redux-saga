import {
  IMAGES_LOAD,
  LOAD_FAIL,
  LOAD_SUCCESS,
  LOAD_FAIL_STATS,
  LOAD_STATS,
  LOAD_SUCCESS_STATS,
} from "../constants";

export const LoadImage = () => ({
  type: IMAGES_LOAD,
});

export const LoadImageSucces = (images) => ({
  type: LOAD_SUCCESS,
  images,
});

export const LoadImageFail = (error) => ({
  type: LOAD_FAIL,
  error,
});

export const loadImageStats = (id) => ({
  type: LOAD_STATS,
  id,
});

export const setImageStats = (id, downloads) => ({
  type: LOAD_SUCCESS_STATS,
  id,
  downloads,
});

export const setImageStatsError = (id) => ({
  type: LOAD_FAIL_STATS,
  id,
});
